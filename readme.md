# Proyecto de evaluación ingeniero Full Stack PHP
___

## El desafío

Queremos jugar al tateti. Ya que somos una empresa que se copa con el tema remoto, queremos jugar al tateti sin que la distancia sea un obstaculo para encontrar al mejor jugador del mundo ;)

Necesitamos tu ayuda para hacer funcionar esta aplicación, tené en cuenta que nosotros ya realizamos el frontend.

#### Funcionalidad del tateti
Los requerimientos funcionales que necesitamos para este proyecto son
  - Que se pueda crear y borrar partidas
  - Que uno se pueda unir a una partida
  - Que se pueda hacer jugadas en la partida
  - Que se identifique un ganador cuando lo haya

#### Que contiene este repositorio
Este es un proyecto Laravel, que contiene un frontend ya previamente desarrollado por nosotros, lo que te pedimos
es que a partir del controlador de maquete desarrolles todo lo necesario para que funcione.

#### Que tenes que codear
 - Todo el backend necesario para que el juego funcione
 - Todos los tests que creas necesarios, considerando que no es necesario probar el frontend.

#### Habilidades que estamos evaluando
 - Habilidades generales de codeo: 
   - Nombre de variables, clases y metodos
   - Como manejás el tema de la performance
   - Cuán legible es el código
   - Cuán simples son los algoritmos que implementás
   - Cómo estructuras el código
 - Habilidades en el modelado
   - Cuales modelos creas para el proyecto
   - Que conceptos intervienen
 - Habilidades para resolver problemas
   - Como solucionas problemas de lógica
 - Conocimiento en PHP
   - Como usas las herramientas que proporciona el lenguaje
   - Que tan bien usas las mejores prácticas que provee PHP
 - Habilidades de desarrollo web
   - Qué patrones pones en practica
   - Que conceptos usas y como
   - Cuales son las mejores practicas que usas
 - Habilidades de testeo
   - Que tipo de pruebas usas
 - Habilidades de GIT
   - Como organizas los cambios
 
 #### Habilidades que no vamos a estudiar
 - Performance en la carga de la aplicacion
 - El estilo, diseño, etc del frontend.
 - Características extras que no pedimos


#### Deadline y otras condiciones
 - Se espera que este ejercicio se resuelva entre 2 y 4 horas.
 - Tenés cuatro dias para mandarnos la solucion
 - Tenes que hacer un fork de este repo y mandarnos en link del fork
 - El código debe estar en inglés

## Ejecutando la aplicacion
[Install docker and docker-compose](https://docs.docker.com/compose/install/)

Correr en bash
```bash
./setup
```
Esto tomara unos minutos, se guardó el deploying tal como estaba no se hicieron cambios, y es por eso
que por una cuestión de versiones nuevas hay que hacer algunos cambios.
Por ejemplo, en el Dockerfile, modificar mysql-client por mariadb-client.
Y otra cosa importante a considerar es que la base de datos en el .env se configura en el default 3306 pero
en el Docker-compose.yml esta configurado para bindear contra 33061 por tanto ojo con eso!, configurar bien
el .env.


Una vez finalizado todo correr
```bash
./up
```

Y la apliación estara corriendo en [http://localhost:8080](http://localhost:8080).

**Importante:** Si el puerto 8080 esta en uso, modificar en el docker-compose.yml el puerto correcto del webserver. Por ejemplo para en vez del 8080 usar el 9090 ponemos:
```yaml
ports:
  - 9090:80
``` 

## Desarrollo por Ezequiel Paolillo

### Deploy
Como se mencionó anteriormente por una cuestión de versionado de los distintos componentes algunos pasos del deploy se deben modificar, no obstante en este readme deje indicado que cosas son para que la próxima pueda levantarse sin mayor problema.

### Desarrollo
El enunciado fue muy claro y conciso, lo primero que uno nota es la ausencia de un modelo que de respuesta al controlador principal (MatchController) por tanto el primer paso es diseñar este modelo e implementarlo
A partir del mockup de los metodos de MatchController es posible empezar a constuir algunos test unitarios sobre
los metodos principales que ya vienen por default, se comenzó por el de traer los Match existentes, crear, borrar, realizar jugada etc.

Un punto importante es que es necesario realizar una pequeña modificación en el frontend para cuando uno realiza una jugada pasarle al backend de que jugador se trata. Esto se señalo en los commits como un punto evolutivo ya que lo ideal seria guardarlo en la sesión, la cual bien podria estar sobre una base Redis. Como el desarrollo excede el concepto de proyecto de evaluación levanto el tópico como punto de mejora, ya que ademas hay un tema de seguridad (utilizando un proxy local podrias modificar que usuario sos y jugar por otro).

Respecto a la funcion que reconoce un ganador, bien se podria haber hecho usando unas cuantas estructuras de condiciones, pero decidí que era una solución por un lado obvia y por otro, dificil de mantener, comprender, y que no sumaba mucho al proyecto. Por tanto fui por el lado de armar un set de máscaras de las jugadas que serian ganadoras e implementé un algoritmo que hace matchear estas mascaras contra las jugadas de cada jugador. De esta forma el dia de mañana podríamos tener un tateti de 4x4, 5x5, 6x6 etc simplemente modificando las máscaras de forma mucho mas visual e intuitiva, incluso podríamos "crear" nuevas formas de ganar como por ejemplo el jugador que arma el glider:

![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Glider.svg/130px-Glider.svg.png)

Sin incurrir en un laberinto de Ifs && ||

### Puntos de mejora
* Creo que se podria charlar la posibilidad de enviar las funciones de cambio de jugador y isWinner a un service provider a fin de alivianar al controlador.
* Utilizar sesiones para los jugadores, autenticación
* La base de datos de Matches deberia estar en un MongoDB
* Las sesiones deberian estar en un Redis
* Se podria migrar el polling de Vue.js a un websocket (Nativo en Vue)

### Uso de GIT
Para el desarrollo se utilizó GIT, por una cuestion de simplicidad en los pasos y de secuencialidad en cada feature hice todo sobre el mismo branch, intenté documentar correctamente tanto el codigo como el GIT a fín de sumar en claridad.

### Testing
Se definieron 5 pruebas unitarias que se pueden ejecutar simplemente haciendo en la consola
```bash
./vendor/bin/phpunit
```

### Demo time!!!
![](https://media.tenor.com/images/bd3b4b22e7490503ea3580aa07d15764/tenor.gif)

http://157.245.214.36:8080/#

Se levantó la aplicación en un VPS en Digital Ocean sobre un Ubuntu 18.04 utilizando todo el stack provisto (Docker, Bash script se setup y run, etc) incluyendo la base de datos MySQL.


### Conclusiones
Les agradezco mucho el tiempo empleado en revisar este trabajo práctico, asi como en considerar mi postulación!!!
