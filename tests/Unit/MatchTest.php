<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MatchTest extends TestCase
{
    /**
     * Test create a Match from zero
     *
     * @return void
     */
    public function testCreate()
    {
        $response = $this->json('POST','api/match',[]);
        $response->assertJsonStructure([
              '*' => ['id', 'name', 'next', 'winner', 'board']
        ]);
    }
    public function testShow()
    {
        $response = $this->json('GET','api/match',[]);
        $response->assertJsonStructure([
              '*' => ['id', 'name', 'next', 'winner', 'board']
        ]);
    }
    
    public function testMove()
    {
        $last_match = \App\Match::orderBy('created_at', 'desc')->get()->first()->id;
        $response = $this->json('PUT','api/match/'.$last_match,["position" => 0, "player" => 2]);
        $response = $this->json('PUT','api/match/'.$last_match,["position" => 1, "player" => 2]);
        $response = $this->json('PUT','api/match/'.$last_match,["position" => 2, "player" => 2]);
        $response = $this->json('PUT','api/match/'.$last_match,["position" => 3, "player" => 1]);
        $response = $this->json('PUT','api/match/'.$last_match,["position" => 4, "player" => 1]);
        $response = $this->json('PUT','api/match/'.$last_match,["position" => 5, "player" => 0]);
        $response = $this->json('PUT','api/match/'.$last_match,["position" => 6, "player" => 0]);
        $response = $this->json('PUT','api/match/'.$last_match,["position" => 7, "player" => 0]);
        $response = $this->json('PUT','api/match/'.$last_match,["position" => 8, "player" => 0]);

        $response->assertJson(
            [
                'id' => $last_match,
                'name' => 'Match'.$last_match,
                'next' => 2,
                'winner' => 0,
                'board' => [2,2,2,1,1,0,0,0,0]
            ]
        );
    }

    public function testGanador(){
        $last_match = \App\Match::orderBy('created_at', 'desc')->get()->first()->id;
        $ganador = \App\Http\Controllers\MatchController::isWinner($last_match);
        $this->assertEquals(2,$ganador);

    }
    
    public function testDelete(){
        $last_match = \App\Match::orderBy('created_at', 'desc')->get()->first()->id;
        $response = $this->json('DELETE','api/match/'.$last_match)->assertStatus(200);
    }

    

}
