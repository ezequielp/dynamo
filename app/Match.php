<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $table = 'matches';
    protected $fillable = ['id', 'name', 'next', 'winner', 'board'];
    public $timestamps = true;

    /**
     * Create a match from zero
     * @return void
     */
    public function create()
    {
        $match = new Match;
            
        $match->name = 'Match';
        $match->next = 1;
        $match->winner = 0;
        $match->board = json_encode([
            0,0,0,
            0,0,0,
            0,0,0,
        ]);
    
        // Save instance
        $match->save();

        // Update naming
        $match->name = $match->name.$match->id;
        $match->save();

        return $match->id;
    }


}
