<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Match;

class MatchController extends Controller {

    public function index()
    {
        return view('index');
    }

    /**
     * Get list of all matches
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function matches()
    {

        // Get All matches
        $matches = Match::all();

        $matches = $matches->map(function($item, $key) {
            $item->board = json_decode($item->board);
            return $item;
        });

        return response()->json($matches);
    }

    /**
     * Get the status of match
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function match($id)
    {

        // Find the board of match
        $match = Match::find($id);
        $match->board = json_decode($match->board); 

        // Give format and return
        return response()->json($match);
    }

    /**
     * Make a move, for that receive a position and player
     *
     * @param $id
     * @param $player
     * @return \Illuminate\Http\JsonResponse
     */
    public function move($id)
    {
        
        
        $position = Input::get('position');
        $player = Input::get('player');

        // Find and update the current board
        $match = Match::find($id);
        $board = json_decode($match->board);
        $board[$position] = $player;
        $match->board = json_encode($board);
        $match->next = $this->changePlayer($player);

        $match->save();

        // If there winner update the match
        if($winner = $this->isWinner($match->id)){
            $match->winner = $winner;
            $match->save();
        }

        return response()->json([
            'id' => $id,
            'name' => 'Match'.$id,
            'next' => $this->changePlayer($player),
            'winner' => 0,
            'board' => json_decode($match->board,false)
        ]);
    }

    /**
     * Create a new Match and return a list of all existents
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $match = new Match;
        $match = $match->create();

        return response()->json(Match::all());
    }

    /**
     * Delete a match
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        Match::find($id)->delete();
        return response()->json(Match::all());
    }

    /**
     * Check if player is winner.
     * 
     * I use a list of eight possible ways to win, creating a set of mask.
     * I know what is possible to resolve this problem using conditional structures
     * but i want to create something different. This is way is very used for
     * high performance applications, in this case is a trivial solution.
     * 
     * @param $id
     * @return Int
     */
    public static function isWinner($id)
    {
        $mask = Array();

        $mask[]= [1,0,0,
                 1,0,0,
                 1,0,0];

        $mask[]= [0,1,0,
                 0,1,0,
                 0,1,0];

        $mask[]= [0,0,1,
                 0,0,1,
                 0,0,1];

        $mask[]= [1,0,0,
                 0,1,0,
                 0,0,1];

        $mask[]= [0,0,1,
                 0,1,0,
                 1,0,0];

        $mask[]= [1,1,1,
                 0,0,0,
                 0,0,0];

        $mask[]= [0,0,0,
                 1,1,1,
                 0,0,0];

        $mask[]= [0,0,0,
                 0,0,0,
                 1,1,1];

        
        $board = json_decode(Match::find($id)->board);


        foreach($mask as $mas){
            $total[0] = 0;
            $total[1] = 0;
            $total[2] = 0;

            $i = 0;
            foreach($mas as $el){
                if(($el * $board[$i]) >= 1){
                    $total[$board[$i]] += 1;
                }
                $i++;
            }
            if($total[1] == 3){
                return 1;
            }
            if($total[2] == 3){
                return 2;
            }
        }
        return 0;

    }

    /**
     * Swap current player
     *
     * @param $player
     * @return Int
     */
    function changePlayer($player)
    {
        if($player == 2){
            return 1;
        }
        else{
            return 2;
        }
    }

}